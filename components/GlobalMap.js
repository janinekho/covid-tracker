import React,{useState, useRef, useEffect}from 'react';
import {Form, Button, Alert, Row, Col} from 'react-bootstrap';
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;

export default function globalMap({country}){
	//States for the mapbox properties
	
    
    const [latitude, setLatitude] = useState(0); //vertical
    const [longitude, setLongitude] = useState(0);//horizontal 
    const [zoom, setZoom] = useState(3);
    const globalMapContainerRef= useRef(null);
    console.log(country)
    useEffect(()=>{

      	fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
         .then(res => res.json())
         .then(data =>{
         	console.log(data);
         	setLongitude(data.features[0].center[0]);
         	setLatitude(data.features[0].center[1]);
         	setZoom(3);
         })

        const map = new mapboxgl.Map({
        	container: globalMapContainerRef.current,
         	style:'mapbox://styles/mapbox/streets-v11',
         	center: [longitude, latitude],
         	zoom: zoom
         })

       map.addControl(new mapboxgl.NavigationControl(),'bottom-right')
    	const marker = new mapboxgl.Marker()
        .setLngLat([longitude, latitude])
        .addTo(map)
    		return() => map.remove()    

		},[latitude, longitude])

    return(
    	<Col xs={12} md={6} lg={12}>
			  <div className="GlobalmapContainer" ref={globalMapContainerRef} />
			 </Col>
    	)

    }




