//Function for converting string data from the end point to numbers
export default function toNum(str){
	//originally for ex. string will be 23,143,197
    //Convert the string to an array to access array methods
    const stringArray = [...str]
    //[2, 3,,,1,4,3...] < it will now spread them. it will now be individual strings
    //grab all numbers and then tanggalin yung comma(,) >>
    //Filter out the commas in the string
    const filteredArr = stringArray.filter(element => element !== ",");
    //["2", "3", "1", "4", "3" ...]
    // reduce this and make it a single string nalang
    //it will concatinate 
    
    // Reduce the filtered awway to a single string without a comma
    //parseInt() converts the string into a number
    return parseInt(filteredArr.reduce((x,y) => x + y))
    //1st iteration - x is the reduced value, y is the next elemetn
    //x = "", y = "2"(equate to the first element)
    //x="2"

    //2nd iteration
    //x="2" y="3"
    //x= "23"

    //3rd Iteration
    //x = "23" //y= "1"
    //x= "231"
    //It will do tis to all of the numbers hangang maubos (loop)
    //final result= "23143197"
}