import React from 'react';
import toNum from '../../../helpers/toNum';
import DoughnutChart from '../../../components/DoughnutChart';
import Banner from '../../../components/Banner';
import GlobalMap from '../../../components/GlobalMap';

export default function Country({country}){
	return(
		<React.Fragment>
		<Banner
        	country={country.country_name}
        	deaths={country.deaths}
        	critical={country.serious_critical}
        	recoveries={country.total_recovered}
		/>
		<DoughnutChart
            criticals={toNum(country.serious_critical)}
            deaths={toNum(country.deaths)}
            recoveries={toNum(country.total_recovered)}
		/>

		<GlobalMap
        country={country.country_name}
		/>
		
		</React.Fragment>
		)
}

export async function getStaticPaths(){
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })

  const data = await res.json()

  const paths = data.countries_stat.map(country =>({
  	params: {id:country.country_name}
  }))

  return{paths, fallback:false}

 }

 //covid/countries/{country_name}
 //covid/countries/:id - ganito din yung idea kung paano naretrieve yung data params..
 //params is a url- it will automatically assign yung id sa link
 /*
 {
	params{
	id: country_name
	},
	fallback:false;
 }
*/
 export async function getStaticProps({params}){
 	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })

  const data = await res.json()

  const country = data.countries_stat.find(country=> country.country_name === params.id)
  return{
  	props:{
  		country
  	}
  }
 }

