import React,{useState, useRef, useEffect}from 'react';
import {Form, Button, Alert, Row, Col} from 'react-bootstrap';
import mapboxgl from 'mapbox-gl';
import { Doughnut } from 'react-chartjs-2';
import toNum from '../../helpers/toNum';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;
export default function Top({data}) {

    // Get array of countries' stats
    const countriesStats = data.countries_stat;
  /*  console.log(countriesStats);*/
    const targetCountry= countriesStats.country_name
    

    // Only get the name and cases of each country in our array
    const countriesCases = countriesStats.map(countryStat => {
        return {
            name: countryStat.country_name,
            // Convert string to number
            cases: toNum(countryStat.cases)
        }
    })

   /* console.log(countriesCases);*/

    //States for the mapbox properties
    const mapContainerRef= useRef(null);
    const [latitude, setLatitude] = useState(0); //vertical
    const [longitude, setLongitude] = useState(0);//horizontal 
    const [zoom, setZoom] = useState(0);

    // Sort in descending order of total number of cases
    countriesCases.sort((a, b) => {
        if(a.cases < b.cases){
            return 1
        }else if(a.cases > b.cases){
            return -1
        }else{
            return 0
        }
    })
    

    
    // acases = 1000
    // bcases = 2000
    // [ a, b ]

    // console.log(countriesCases);

    useEffect(()=>{
    const map = new mapboxgl.Map({
        container: mapContainerRef.current,
         style:'mapbox://styles/mapbox/streets-v11',
         center: [44.63, 28 ],
         zoom: zoom
    })

    map.addControl(new mapboxgl.NavigationControl(),'bottom-right')
    
    console.log(countriesCases);
    for(var i = 0; i < 10; i++){
        var longitude1=0 ;
        var latitude1=0;
   console.log(countriesCases[i].name);
  
    fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${countriesCases[i].name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY}`)
        

        .then(res => res.json())
        .then(data =>{
            console.log(data);

            longitude1=(data.features[0].center[0]);
            latitude1=(data.features[0].center[1]);
            setZoom(1);
        
        

    const marker = new mapboxgl.Marker()
        .setLngLat([longitude1, latitude1])
        .addTo(map)


        })
    }


   /* const marker = new mapboxgl.Marker()
        .setLngLat([longitude, latitude])
        .addTo(map)*/
      

},[latitude, longitude])

    return (
        <React.Fragment>
            <h1>10 Countries With The Highest Number of Cases</h1>
            <Doughnut data={{
                datasets: [
                    {
                        data: [countriesCases[0].cases, countriesCases[1].cases, countriesCases[2].cases, countriesCases[3].cases, countriesCases[4].cases, countriesCases[5].cases, countriesCases[6].cases, countriesCases[7].cases, countriesCases[8].cases, countriesCases[9].cases],
                        backgroundColor: ["#696969", "#bada55", "#7fe5f0", "#ff0000", "#ff80ed", "#407294", "#cbcba9", "#420420", "#133337", "#065535"]
                    }
                ],
                labels: [countriesCases[0].name, countriesCases[1].name, countriesCases[2].name, countriesCases[3].name, countriesCases[4].name, countriesCases[5].name, countriesCases[6].name, countriesCases[7].name, countriesCases[8].name, countriesCases[9].name]
            }} redraw={ false }/>
            <Col xs={12} md={6} lg={12}>
              <div className="mapContainer" ref={mapContainerRef} />
             </Col>
        </React.Fragment>
    )
}

export async function getStaticProps() {

    // Fetch data from the /courses API endpoint    
    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
            "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
        }
    })
    const data = await res.json()
  
    // Return the props
    return { 
        props: {
            data
        }
    }
}
