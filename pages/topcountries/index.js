import React, {useState} from 'react';
import { Doughnut } from 'react-chartjs-2';
import {Form, Button, Alert} from 'react-bootstrap';
import toNum from '../../helpers/toNum';


export default function Top({data}){
    


    


  const TopStats = data.countries_stat.map(country =>{
     return{
       name: country.country_name,
       cases: toNum(country.cases)

   }
  })

  console.log(TopStats);




  return(
   <React.Fragment>
    <h1> Top 10 Countries With The Highest Number of Case </h1>
    <Doughnut data={{
          datasets: [{
            data:[TopStats[0].cases, TopStats[1].cases, TopStats[2].cases, TopStats[3].cases, TopStats[4].cases, TopStats[5].cases, TopStats[6].cases, TopStats[7].cases, TopStats[8].cases, TopStats[9].cases],
            backgroundColor: ["red", "orange", "green","blue","purple","yellow", "indigo", "black","pink","grey"]
          }],
          labels: [TopStats[0].name, TopStats[1].name, TopStats[2].name, TopStats[3].name, TopStats[4].name, TopStats[5].name, TopStats[6].name, TopStats[7].name, TopStats[8].name, TopStats[9].name]
    }} redraw={false} />
   </React.Fragment>
	) 


}


  

export async function getStaticProps(){
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })

  const data = await res.json()


return {
	props: {
		data
	}
 
}
};